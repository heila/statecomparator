/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The Java Pathfinder core (jpf-core) platform is licensed under the
 * Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package gov.nasa.jpf.vm;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPFConfigException;
import gov.nasa.jpf.vm.serialize.DebugMarkedCFSerializer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * a JenkinsStateSet that stores program state information in a readable and diffable format.
 * 
 * Storing states as readable text is enabled by setting vm.storage.class to this class
 * 
 * Note: this automatically sets/overrides the serializer to Debug<serializer-class>
 */
public class DebugMarkedJenkinsStateSet extends JenkinsStateSet {

  static final String LOGFILE = "state";
  public static String mTag = null;
  public static StateInfo si;

  File outputDir;

  public DebugMarkedJenkinsStateSet(Config conf) {
    String serializerCls = conf.getString("vm.serializer.class");
    if (serializerCls != null) {
      int idx = serializerCls.lastIndexOf('.') + 1;
      String cname = serializerCls.substring(idx);

      if (!cname.startsWith("DebugMarked")) {
        if (idx > 0) {
          serializerCls = serializerCls.substring(0, idx) + "DebugMarked" + cname;
        } else {
          serializerCls = "DebugMarked" + cname;
        }
      }

      serializer = conf.getInstance(null, serializerCls, DebugMarkedCFSerializer.class);
      if (serializer == null) {
        throw new JPFConfigException("Debug StateSet cannot instantiate serializer: " + serializerCls);
      }
    }

    String path = conf.getString("vm.serializer.output", "tmp");
    outputDir = new File(path);
    if (!outputDir.isDirectory()) {
      if (!outputDir.mkdirs()) {
        throw new JPFConfigException("Debug StateSet cannot create output dir: "
            + outputDir.getAbsolutePath());
      }
    }

  }

  public static void markState(String sTag) {
    mTag = sTag;
  }

  @Override
  public void attach(VM vm) {
    // we use our own serializer
    vm.setSerializer(serializer);

    // <2do> this is a bit hack'ish - why does the VM keep the serializer anyways,
    // if it is only used here
    super.attach(vm);
  }

  @Override
  public int addCurrent() {
    int stateId = -1;
    if (mTag != null) {
      int maxId = size() - 1;

      ByteArrayOutputStream os = new ByteArrayOutputStream();
      ((DebugMarkedCFSerializer) serializer).setOutputStream(os);
      si = new StateInfo();
      stateId = super.addCurrent();

      // if this is a new state, store it under its id, otherwise throw it away
      if (stateId > maxId) {
        String fname = "state_" + mTag + "." + stateId;
        try {
          os.flush();
          File ff = new File(outputDir.getAbsolutePath() + File.separator + fname);
          ff.getParentFile().mkdir();
          ff.createNewFile();
          OutputStream stream = new FileOutputStream(ff, false);
          os.writeTo(stream);
          os.flush();
          os.close();

        } catch (IOException e) {
          e.printStackTrace();
        }
      } else {
        try {
          os.close();
        } catch (IOException e) {
        }
      }
    } else {
      // do not print state
      ((DebugMarkedCFSerializer) serializer).setOutputStream(null);
      stateId = super.addCurrent();

    }
    return stateId;

  }
}
