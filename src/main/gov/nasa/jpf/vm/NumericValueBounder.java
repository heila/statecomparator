/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The Java Pathfinder core (jpf-core) platform is licensed under the
 * Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package gov.nasa.jpf.vm;

import java.util.ArrayList;
import java.util.List;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPFConfigException;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.jvm.bytecode.DSTORE;
import gov.nasa.jpf.jvm.bytecode.FSTORE;
import gov.nasa.jpf.jvm.bytecode.IINC;
import gov.nasa.jpf.jvm.bytecode.ISTORE;
import gov.nasa.jpf.jvm.bytecode.JVMFieldInstruction;
import gov.nasa.jpf.jvm.bytecode.JVMInstruction;
import gov.nasa.jpf.jvm.bytecode.JVMInstructionVisitorAdapter;
import gov.nasa.jpf.jvm.bytecode.JVMLocalVariableInstruction;
import gov.nasa.jpf.jvm.bytecode.LSTORE;
import gov.nasa.jpf.jvm.bytecode.PUTFIELD;
import gov.nasa.jpf.jvm.bytecode.PUTSTATIC;
import gov.nasa.jpf.util.FieldSpec;
import gov.nasa.jpf.util.VarSpec;
import gov.nasa.jpf.vm.bytecode.FieldInstruction;

/**
 * little listener that sets value ranges of specified numeric fields and local vars
 * 
 *
 * configuration examples:
 *
 * range.fields=speed,.. range.speed.field=x.y.SomeClass.velocity range.speed.min=300 range.speed.max=500
 *
 * range.vars=altitude,.. range.altitude.var=x.y.SomeClass.computeTrajectory(int):a range.altitude.min=125000
 *
 * Local variables can not be annotated (compiled away by javac). We can however process fields in this way.
 *
 */
public class NumericValueBounder extends ListenerAdapter {

  static abstract class RangeCheck {
    double min, max;

    RangeCheck(double min, double max) {
      this.min = min;
      this.max = max;
    }

    String check(long v) {
      if (v < (long) min) {
        return String.format("%d < %d", v, (long) min);
      } else if (v > (long) max) {
        return String.format("%d > %d", v, (long) max);
      }
      return null;
    }

    String check(double v) {
      if (v < min) {
        return String.format("%f < %f", v, min);
      } else if (v > (long) max) {
        return String.format("%f > %f", v, max);
      }
      return null;
    }
  }

  static class FieldCheck extends RangeCheck {
    FieldSpec fspec;

    FieldCheck(FieldSpec fspec, double min, double max) {
      super(min, max);
      this.fspec = fspec;
    }

    boolean matches(FieldInfo fi) {
      return fspec.matches(fi);
    }
  }

  static class VarCheck extends RangeCheck {
    VarSpec vspec;

    VarCheck(VarSpec vspec, double min, double max) {
      super(min, max);
      this.vspec = vspec;
    }

    LocalVarInfo getMatch(MethodInfo mi, int pc, int slotIdx) {
      return this.vspec.getMatchingLocalVarInfo(mi, pc, slotIdx);
    }
  }

  class Visitor extends JVMInstructionVisitorAdapter {

    void checkFieldInsn(JVMFieldInstruction insn) {
      ThreadInfo ti = ThreadInfo.getCurrentThread();

      FieldInfo fi = insn.getFieldInfo();

      AnnotationInfo anno = fi.getAnnotation("gov.nasa.jpf.annotation.NumericBound");
      if (anno != null) {

        Object fmin = anno.getValue("min");
        Object fmax = anno.getValue("max");

        FieldCheck fc = new FieldCheck(null, ((fmin != null) ? (Double) fmin : Double.MIN_VALUE),
            ((fmax != null) ? (Double) fmax : Double.MAX_VALUE));

        long lv = insn.getLastValue();
        String errorCond = fi.isFloatingPointField() ? fc.check(Double.longBitsToDouble(lv)) : fc.check(lv);

        if (errorCond != null) {
          Double bound = (errorCond.contains("<")) ? new Double(fc.min) : new Double(fc.max);
          int fieldSize = fi.getStorageSize();
//          ElementInfo ei = ((PUTFIELD) insn).getElementInfo(ti);
          ElementInfo ei2 = ((FieldInstruction) insn).getLastElementInfo();

          if (fieldSize == 1) {
            ei2.set1SlotField(fi, bound.intValue());
          } else {
            ei2.set2SlotField(fi, bound.longValue());
          }
        }
        return;
      } else {
        if (fieldChecks != null) {
          for (FieldCheck fc : fieldChecks) {
            if (fc.matches(fi)) {
              if (fi.isNumericField()) {
                long lv = insn.getLastValue();
                String errorCond = fi.isFloatingPointField() ? fc.check(Double.longBitsToDouble(lv)) : fc
                    .check(lv);

                if (errorCond != null) {
                  Double bound = (errorCond.contains("<")) ? new Double(fc.min) : new Double(fc.max);
                  int fieldSize = fi.getStorageSize();
                  int objRef = ti.getTopFrame().peek((fieldSize == 1) ? 1 : 2);
                  ElementInfo ei = ti.getElementInfo(objRef);
                  if (fieldSize == 1) {
                    ei.set1SlotField(fi, bound.intValue());
                  } else {
                    ei.set2SlotField(fi, bound.longValue());
                  }
                }
              }
            }
          }
        }
      }
    }

    void checkVarInsn(JVMLocalVariableInstruction insn) {
      ThreadInfo ti = ThreadInfo.getCurrentThread();
      StackFrame frame = ti.getTopFrame();
      int slotIdx = insn.getLocalVariableIndex();

      LocalVarInfo lvar = insn.mi.getLocalVar(slotIdx, insn.position);
      if (lvar != null) {
        // AnnotationInfo anno = lvar.getAnnotation("BoundVar");
        // if (anno != null) {
        // VarSpec vs = VarSpec.createVarSpec("*" + insn.mi.getSignature() + ":" + lvar.getName());
        //
        // Object fmin = anno.getValue("min");
        // Object fmax = anno.getValue("max");
        //
        // VarCheck vc = new VarCheck(vs, ((fmin != null) ? (Double) fmin : Double.MIN_VALUE),
        // ((fmax != null) ? (Double) fmax : Double.MAX_VALUE));
        // long v = lvar.getSlotSize() == 1 ? frame.getLocalVariable(slotIdx) : frame
        // .getLongLocalVariable(slotIdx);
        // String errorCond = lvar.isFloatingPoint() ? vc.check(Double.longBitsToDouble(v)) : vc.check(v);
        //
        // if (errorCond != null) {
        // Double bound = (errorCond.contains("<")) ? new Double(vc.min) : new Double(vc.max);
        //
        // if (lvar.getSlotSize() == 1)
        // frame.setLocalVariable(slotIdx, bound.intValue());
        // else
        // frame.setLongLocalVariable(slotIdx, bound.longValue());
        // }
        // return;
        // }
        if (varChecks != null) {

          for (VarCheck vc : varChecks) {
            MethodInfo mi = insn.getMethodInfo();
            int pc = insn.getPosition() + 1; // the scope would begin on the next insn, we are still at the
                                             // xSTORE
            lvar = vc.getMatch(mi, pc, slotIdx);
            if (lvar != null) {
              long v = lvar.getSlotSize() == 1 ? frame.getLocalVariable(slotIdx) : frame
                  .getLongLocalVariable(slotIdx);
              String errorCond = lvar.isFloatingPoint() ? vc.check(Double.longBitsToDouble(v)) : vc.check(v);

              if (errorCond != null) {
                Double bound = (errorCond.contains("<")) ? new Double(vc.min) : new Double(vc.max);

                if (lvar.getSlotSize() == 1)
                  frame.setLocalVariable(slotIdx, bound.intValue());
                else
                  frame.setLongLocalVariable(slotIdx, bound.longValue());
                return;
              }
            }
          }
        }
      }
    }

    void checkVarInsn(IINC insn) {
      ThreadInfo ti = ThreadInfo.getCurrentThread();
      StackFrame frame = ti.getTopFrame();
      int slotIdx = insn.getIndex();

      if (varChecks != null) {
        for (VarCheck vc : varChecks) {
          MethodInfo mi = insn.getMethodInfo();
          int pc = insn.getPosition() + 1; // the scope would begin on the next insn, we are still at the
                                           // xSTORE
          LocalVarInfo lvar = vc.getMatch(mi, pc, slotIdx);
          if (lvar != null) {
            long v = lvar.getSlotSize() == 1 ? frame.getLocalVariable(slotIdx) : frame
                .getLongLocalVariable(slotIdx);
            String errorCond = lvar.isFloatingPoint() ? vc.check(Double.longBitsToDouble(v)) : vc.check(v);

            if (errorCond != null) {
              Double bound = (errorCond.contains("<")) ? new Double(vc.min) : new Double(vc.max);

              if (lvar.getSlotSize() == 1)
                frame.setLocalVariable(slotIdx, bound.intValue());
              else
                frame.setLongLocalVariable(slotIdx, bound.longValue());
              break;
            }
          }
        }
      }
    }

    @Override
    public void visit(PUTFIELD insn) {
      checkFieldInsn(insn);
    }

    @Override
    public void visit(PUTSTATIC insn) {
      checkFieldInsn(insn);
    }

    @Override
    public void visit(ISTORE insn) {
      checkVarInsn(insn);
    }

    @Override
    public void visit(LSTORE insn) {
      checkVarInsn(insn);
    }

    @Override
    public void visit(FSTORE insn) {
      checkVarInsn(insn);
    }

    @Override
    public void visit(DSTORE insn) {
      checkVarInsn(insn);
    }

    @Override
    public void visit(IINC insn) {
      checkVarInsn(insn);
    }

  }

  VM vm;
  Visitor visitor;

  // the stuff we monitor
  List<FieldCheck> fieldChecks;
  List<VarCheck> varChecks;

  String error; // where we store errorCond details

  public NumericValueBounder(Config conf) {
    visitor = new Visitor();

    createFieldChecks(conf);
    createVarChecks(conf);
  }

  private void createFieldChecks(Config conf) {
    String[] checkIds = conf.getCompactTrimmedStringArray("range.fields");
    if (checkIds.length > 0) {
      fieldChecks = new ArrayList<FieldCheck>(checkIds.length);

      for (int i = 0; i < checkIds.length; i++) {
        String id = checkIds[i];
        FieldCheck check = null;
        String keyPrefix = "range." + id;
        String spec = conf.getString(keyPrefix + ".field");
        if (spec != null) {
          FieldSpec fs = FieldSpec.createFieldSpec(spec);
          if (fs != null) {
            double min = conf.getDouble(keyPrefix + ".min", Double.MIN_VALUE);
            double max = conf.getDouble(keyPrefix + ".max", Double.MAX_VALUE);
            check = new FieldCheck(fs, min, max);
          }
        }
        if (check == null) {
          throw new JPFConfigException("illegal field range check specification for " + id);
        }
        fieldChecks.add(check);
      }
    }
  }

  private void createVarChecks(Config conf) {
    String[] checkIds = conf.getCompactTrimmedStringArray("range.vars");
    if (checkIds.length > 0) {
      varChecks = new ArrayList<VarCheck>(checkIds.length);

      for (int i = 0; i < checkIds.length; i++) {
        String id = checkIds[i];
        VarCheck check = null;
        String keyPrefix = "range." + id;
        String spec = conf.getString(keyPrefix + ".var");
        if (spec != null) {
          VarSpec vs = VarSpec.createVarSpec(spec);
          if (vs != null) {
            double min = conf.getDouble(keyPrefix + ".min", Double.MIN_VALUE);
            double max = conf.getDouble(keyPrefix + ".max", Double.MAX_VALUE);
            check = new VarCheck(vs, min, max);
          }
        }
        if (check == null) {
          throw new JPFConfigException("illegal variable range check specification for " + id);
        }
        varChecks.add(check);
      }
    }
  }

  @Override
  public void instructionExecuted(VM vm, ThreadInfo ti, Instruction nextInsn, Instruction executedInsn) {
    this.vm = vm;
    ((JVMInstruction) executedInsn).accept(visitor);
  }

}
