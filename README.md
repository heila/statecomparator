Overview
=======

Model checking software applications can result in exploring large or infinite state spaces. It is thus essential to identify and abstract variables that can take on a large number of values, in order to increase state matching. StateComparator is an extension to Java PathFinder which compares states in the state space to identify variables that should be abstracted to reduce a too large on infinite state space.


Usage
=======

 1. Include StateComparator in you JPF config file:

    `@using statecomparator`

 2. Add mark-state statements in you project at point where you expect state matching.

    For jpf-android projects:

    `AndroidStateComparator.markState(String TAG, int startAfter, int stopAfter);`


    For Java projects:

    `StateComparator.markState(String TAG, int startAfter, int stopAfter);`

 3.  Run the project on JPF. If it is a concurrent project you might want to only execute a single thread scheduling. The can be done by configuring the vm.scheduler.sync.class to the Android SyncPolicy in the config file.  

    The tool outputs a file statematching.txt file reporting the changes for all marked state comparisons:
  
        === COMPARING STATE 5 TO  STATE 1 ====
        FirstTask.count: (0 ==> 1)
        Object Trace:
        @246 object FirstTask mFields={count=1, event1=X, event2=@242, group=X, interrupted=X, isDaemon=X, name=X, parkBlocker=X, permit=X, priority=X, stopException=X, target=null, threadLocalRandomProbe=X, threadLocalRandomSecondarySeed=X, threadLocalRandomSeed=X, threadLocals=X, uncaughtExceptionHandler=X}
        @1 frame FirstTask.run(...) locals={this=@246}

        Event.count: (0 ==> 1)
        Object Trace:
        @242 object Event mFields={count=1}
        @246 object FirstTask mFields={count=1, event1=X, event2=@242, group=X, interrupted=X, isDaemon=X, name=X, parkBlocker=X, permit=X, priority=X, stopException=X, target=null, threadLocalRandomProbe=X, threadLocalRandomSecondarySeed=X, threadLocalRandomSeed=X, threadLocals=X, uncaughtExceptionHandler=X}
        @1 frame FirstTask.run(...) locals={this=@246}
        
        === COMPARING STATE 10 TO  STATE 5 ====
        FirstTask.count: (1 ==> 2)
        Object Trace:
         @246 object FirstTask mFields={count=2, event1=X, event2=@242, group=X, interrupted=X, isDaemon=X, name=X, parkBlocker=X, permit=X, priority=X, stopException=X, target=null, threadLocalRandomProbe=X, threadLocalRandomSecondarySeed=X, threadLocalRandomSeed=X, threadLocals=X, uncaughtExceptionHandler=X}
        @1 frame FirstTask.run(...) locals={this=@246}
        
        Event.count: (1 ==> 2)
        Object Trace:
        @242 object Event mFields={count=2}
        @246 object FirstTask mFields={count=2, event1=X, event2=@242, group=X, interrupted=X, isDaemon=X, name=X, parkBlocker=X, permit=X, priority=X, stopException=X, target=null, threadLocalRandomProbe=X, threadLocalRandomSecondarySeed=X, threadLocalRandomSeed=X, threadLocals=X, uncaughtExceptionHandler=X}
        @1 frame FirstTask.run(...) locals={this=@246}
    
        SecondTask.count: (0 ==> 1)
        Object Trace:
        @258 object SecondTask mFields={count=1, event1=X, event2=@242, group=X, interrupted=X, isDaemon=X, name=X, parkBlocker=X, permit=X, priority=X, stopException=X, target=null, threadLocalRandomProbe=X, threadLocalRandomSecondarySeed=X, threadLocalRandomSeed=X, threadLocals=X, uncaughtExceptionHandler=X}
        @1 frame SecondTask.run(...) locals={this=@258}

    and a  tmp/ directory that contains a serialized version of the marked states for manual inspection using a diff tool like meld.


 4. The are a few ways to remove fields from the state:

    * @FilterField annotation to fields (can take a condition)

    * reflection in config file: 

            filter.ignoreFields=BinaryTree.modCount

    * @NumericBound(max = 1020) or in config file: 

            listener+=;gov.nasa.jpf.vm.NumericValueBounder;
            range.vars=x
            range.x.var=SimpleExample.main:iTest
            range.x.max=1002

    * AbstractionAdapter:   
    
            public class MyClassDataAbstraction extends AbstractionAdapter {  
               @Override  
               public int getAbstractValue(int data) {  
                    if (data >= 4)  
                        return 4;  
                    return data;  
               }  
            }
        

             