package gov.nasa.jpf.vm;

import java.util.Map;

public class StackFrameInfo extends Info {
  public int depth;
  public int threadID;
  public String mClassName;
  public String mMethodName;
  public Map<String, Object> mLocals;
  public boolean isStatic = false;

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + depth;
    result = prime * result + ((mClassName == null) ? 0 : mClassName.hashCode());
    result = prime * result + ((mMethodName == null) ? 0 : mMethodName.hashCode());
    result = prime * result + threadID;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    StackFrameInfo other = (StackFrameInfo) obj;
    if (depth != other.depth)
      return false;
    if (mClassName == null) {
      if (other.mClassName != null)
        return false;
    } else if (!mClassName.equals(other.mClassName))
      return false;
    if (mMethodName == null) {
      if (other.mMethodName != null)
        return false;
    } else if (!mMethodName.equals(other.mMethodName))
      return false;
    if (threadID != other.threadID)
      return false;
    return true;
  }

  @Override
  public String toString() {
    StringBuffer ps = new StringBuffer();

    ps.append('@');
    ps.append(depth);

    ps.append(" frame ");
    ps.append(mClassName + "." + mMethodName);
    ps.append("(...) ");

    if (mLocals != null) {
      ps.append("locals=");
      ps.append(mLocals.toString());
    }

    return ps.toString();
  }
}
