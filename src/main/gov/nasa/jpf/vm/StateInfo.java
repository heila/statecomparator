package gov.nasa.jpf.vm;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class StateInfo {

  int mId;
  String mTag;

  public Map<Integer, ObjectInfo> dMap = new HashMap<Integer, ObjectInfo>();
  public Map<Integer, StaticInfo> sMap = new HashMap<Integer, StaticInfo>();
  public Set<StackFrameInfo> fMap = new HashSet<StackFrameInfo>();

  /**
   * Compares the object hierarchies of two states. The roots of these hierarchies are the static fields and
   * the local fields in the currently active stackframes.
   * 
   * @param si
   * @param writer
   * @throws Exception
   */
  public void compare(StateInfo si, PrintWriter writer) throws Exception {
    writer.println("\n=== COMPARING STATE " + mId + " TO  STATE " + si.mId + " ====");

    compareStaticVars(si, writer);
    compareStackFrames(si, writer);
  }

  public void compareStaticVars(StateInfo si, PrintWriter writer) throws Exception {

    // for each class
    for (ObjectInfo objPrev : si.sMap.values()) {

      // clear all marked objects
      clearMarkedObjects(si.dMap);
      clearMarkedStatics(si.sMap);

      // get matching class
      ObjectInfo objCurr = this.sMap.get(objPrev.mRef);

      if (objCurr != null && similar(objPrev, objCurr)) { // sanity check
        // recursively compare vars
        compareObjects(objPrev, objCurr, this, si, writer);
      } else {
        // impossible (class can not be unloaded in a path)
      }
    }
  }

  public void compareStackFrames(StateInfo si, PrintWriter writer) throws Exception {

    // for each object recursively compare
    for (StackFrameInfo objPrev : this.fMap) {

      // clear all marked objects
      clearMarkedObjects(this.dMap);

      StackFrameInfo objNext = null;
      for (StackFrameInfo obj : si.fMap) {
        if (objPrev.equals(obj)) {
          objNext = obj;
          break;
        }
      }

      if (objNext != null) {
        // recursively compare fields/ objects
        compareFrameReferences(objPrev, objNext, this, si, writer);

      } else {
        // removed object - not in current state
      }
    }

  }

  private void compareFrameReferences(StackFrameInfo objPrev, StackFrameInfo objNext, StateInfo prevSI,
                                      StateInfo currentSI, PrintWriter pw) throws Exception {

    boolean match = true;
    for (Entry<String, Object> entry : objPrev.mLocals.entrySet()) {
      Object obj = entry.getValue();
      Object obj2 = objNext.mLocals.get(entry.getKey());

      if (isNull(obj)) {
        if (!isNull(obj2)) {
          match = notMatching(pw, objPrev.mClassName + "." + objPrev.mMethodName + " " + entry.getKey()
              + ": (" + obj2 + "==>" + obj + ")", objPrev, objNext);
        } else {
          // both fields are null and thus equal, continue to next
          continue;
        }
      } else if (isNull(obj2)) {
        match = notMatching(pw, objPrev.mClassName + "." + objPrev.mMethodName + "()." + entry.getKey()
            + ": (" + obj2 + "==>" + obj + ")", objPrev, objNext);

        // break;
      } else if (isReference(obj)) {
        if (isReference(obj2)) {
          // get objectInfo's
          ObjectInfo origOI = getObjectInfo(prevSI, obj);
          ObjectInfo newOI = getObjectInfo(currentSI, obj2);
          if (origOI != null && newOI != null && !origOI.mVisited) {
            origOI.mParent = objPrev;
            newOI.mParent = objNext;
            match = compareObjects(origOI, newOI, prevSI, currentSI, pw);
          } else {
            // this should not happen - means that reference object does not exist in heap????
            continue;
          }
        } else {
          match = notMatching(pw, objPrev.mClassName + "." + entry.getKey() + ": (" + obj + "==>" + obj2
              + ")", objPrev, objNext);
        }
      } else {
        if (!obj.equals(obj2)) {
          match = notMatching(pw, objPrev.mClassName + "." + entry.getKey() + ": (" + obj + "==>" + obj2
              + ")", objPrev, objNext);

          break;
        }
      }
    }

  }

  private void clearMarkedObjects(Map<Integer, ObjectInfo> prev) {
    for (ObjectInfo objPrev : prev.values()) {
      objPrev.mVisited = false;
    }
  }

  private void clearMarkedStatics(Map<Integer, StaticInfo> prev) {
    for (StaticInfo objPrev : prev.values()) {
      objPrev.mVisited = false;
    }

  }

  /**
   * Recursively compare object hierarchies starting from prevOI and currentOI. Mark objects as visited to
   * avoid infinte loop. None of the objects must be null!
   * 
   * @param prevOI
   *          - the previous state's object root
   * @param currentOI
   *          - the current state's object root
   * @param prevSI
   *          - previous state
   * @param currentSI
   *          - current state
   * @param pw
   *          - to write output
   * @return if objects matched
   */
  private boolean compareObjects(ObjectInfo prevOI, ObjectInfo currentOI, StateInfo prevSI,
                                 StateInfo currentSI, PrintWriter pw) throws Exception {

    // assume match
    boolean match = true;

    // sanity check
    if (prevOI == null || currentOI == null || prevSI == null || currentSI == null) {
//      throw new Exception("Argument is null");
    return false;
    }

    // avoid recursion
    prevOI.mVisited = true;

    if (prevOI.mType.equals("object") || (prevOI.mType.equals("class"))) {

      // match fields (filename, value/ref)
      for (Entry<String, Object> fEntryOrig : prevOI.mFields.entrySet()) {

        // value of fields
        Object fValueOrig = fEntryOrig.getValue();
        Object fValueNew = currentOI.mFields.get(fEntryOrig.getKey());

        // if orig value is null
        if (isNull(fValueOrig) && isNull(fValueNew)) {
          continue;
        } else if (isNull(fValueOrig) || isNull(fValueNew)) {
          match = notMatching(pw, prevOI.mClassName + "." + fEntryOrig.getKey() + ": (" + fValueNew + " ==> "
              + fValueOrig + ")", prevOI, currentOI);

        } else if (isReference(fValueOrig)) {

          // get objectInfo's referenced
          ObjectInfo origOI = getObjectInfo(prevSI, fValueOrig);
          ObjectInfo newOI = getObjectInfo(currentSI, fValueNew);

          if (origOI != null) {
            if (!origOI.mVisited) {
              origOI.mParent = prevOI;
              match = compareObjects(origOI, newOI, prevSI, currentSI, pw);
            }
          } else {
            System.out.println("Object not found in state:" + prevSI.mId + " with reference: " + fValueOrig);
          }

        } else if (!fValueOrig.equals(fValueNew)) { // primitive value in wrapper class
          match = notMatching(pw, prevOI.mClassName + "." + fEntryOrig.getKey() + ": (" + fValueNew + " ==> "
              + fValueOrig + ")", prevOI, currentOI);
        }
      }

    } else if (prevOI.mType.equals("array")) {

      // sanity check - array has fixed length
      if (prevOI.mList.length != currentOI.mList.length) {
        pw.println();
        match = notMatching(pw, "LIST " + prevOI.mClassName + ": " + prevOI.mList
            + " --- LENGTH NOT MATCHING --- " + currentOI.mList.length, prevOI, currentOI);
      } else {
        for (int i = 0; i < prevOI.mList.length; i++) {
          Object obj = prevOI.mList[i];
          Object obj2 = currentOI.mList[i];

          if (isNull(obj)) {
            if (!isNull(obj2)) {
              match = notMatching(pw, "LIST ITEM " + i + ": " + obj + " --- NOT MATCHING --- " + obj2,
                  prevOI, currentOI);
            } else {
              // both fields are null and thus equal, continue to next
              continue;
            }
          } else if (isNull(obj2)) {
            match = notMatching(pw, "LIST ITEM " + i + ": " + obj + " --- NOT MATCHING --- " + obj2, prevOI,
                currentOI);
          } else if (isReference(obj) && isReference(obj2)) {
            // get objectInfo's
            ObjectInfo origOI = getObjectInfo(prevSI, obj);
            ObjectInfo newOI = getObjectInfo(currentSI, obj2);
            if (origOI != null && newOI != null && !origOI.mVisited) {
              origOI.mParent = prevOI;
              newOI.mParent = currentOI;
              match = compareObjects(origOI, newOI, prevSI, currentSI, pw);
            } else {
              // this should not happen - means that reference object does not exist in heap????
              continue;
            }
          } else {
            if (!obj.equals(obj2)) {
              match = notMatching(pw, "LIST ITEM " + i + ": " + obj + " --- NOT MATCHING --- " + obj2,
                  prevOI, currentOI);
              break;
            }
          }
        }
      }
    }

    return match;

  }

  private boolean isReference(Object value) {
    return value instanceof String && ((String) value).startsWith("@");
  }

  private boolean isNull(Object obj) {
    return obj == null || obj.equals("null");
  }

  private ObjectInfo getObjectInfo(StateInfo si, Object obj) {
    if (obj != null && obj instanceof String) {
      String sref = ((String) obj).substring(1);
      int iref = Integer.parseInt(sref, 16);
      return si.dMap.get(iref);
    } else {
      // problem
    }
    return null;
  }

  private boolean notMatching(PrintWriter pw, String message, Info prevOI, Info currentOI) {
    if (prevOI.mMatch) {
      pw.println("\n" + message);
      pw.println("Object Trace:");
      prevOI.printTrace(pw);
      prevOI.mMatch = false;
    }
    return false;
  }

  /**
   * Checks that type, classname and if array list length is the same
   * 
   * @param prevOI
   * @param currentOI
   * @return
   */
  private boolean similar(ObjectInfo prevOI, ObjectInfo currentOI) {
    boolean sameApprox = prevOI.mType.equals(currentOI.mType)
        && prevOI.mClassName.equals(currentOI.mClassName);
    if (prevOI.mList != null) {
      sameApprox = sameApprox && (prevOI.mList.length == currentOI.mList.length);
    }
    return sameApprox;
  }

}
