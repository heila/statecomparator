package gov.nasa.jpf.vm;

public class StateComparator {

  public static native void markState(String tag);

  public static native void markState(String tag, int max);

  public static native void markState(String tag, int max, int startAfter);
}
