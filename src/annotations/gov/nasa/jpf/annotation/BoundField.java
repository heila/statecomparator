package gov.nasa.jpf.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that a field in the model should not be considered during state matching.
 * 
 * @author peterd
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.LOCAL_VARIABLE })
public @interface BoundField {

  /**
   * If not the empty string, specifies a property that must be "true" to activate this filter--unless
   * <code>invert</code> is set.
   */
  int max() default 1000;

  /**
   * If not the empty string, specifies a property that must be "true" to activate this filter--unless
   * <code>invert</code> is set.
   */
  int min() default -1000;

}
