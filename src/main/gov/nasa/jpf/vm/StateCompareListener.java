package gov.nasa.jpf.vm;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.util.StateExtensionClient;
import gov.nasa.jpf.util.StateExtensionListener;
import gov.nasa.jpf.vm.StateCompareListener.StateNode;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * When the code passes over a Verify.TAG the current state is tagged and pushed on a stack for this path in
 * the search tree. When more than one TAG'ed state appears in this stack we compare the next tagged state
 * with the previous state. and print out the difference.
 * 
 * @author Heila van der Merwe
 * 
 */
public class StateCompareListener extends ListenerAdapter implements StateExtensionClient<StateNode> {
  public static class StateNode {
    StateInfo prevState;
    int depth;
    int count;
  }

  PrintWriter mWriter;

  /** Counts the actual number of tagged states */
  public static int mCount = Integer.MAX_VALUE;
  private int count = 0;
  
  public static String mTag = null;
  
  public static int mStartAfter = 0;
  
  /** Counts the number of TAGS encountered in a path */
  private int depth = 0;
  
  StateInfo prevState = null;

  public static boolean isAndroid = false;

  public StateCompareListener(Config config) {
    try {
      mWriter = new PrintWriter(Files.newBufferedWriter(Paths.get("statematching.txt")));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void stateAdvanced(Search search) {

    if (search.isVisitedState()) {
      mWriter.println("\n=== REACHED VISITED STATE " + search.getStateId() + " ===");
      // we don't want to compare to previously visited state
      return;
    }

    if (mTag != null) {
      // this state should be tagged

      if (isAndroid
          && (!search.getVM().getChoiceGenerator().getClass().getName()
              .contains("AndroidEventChoiceGenerator"))) {
        // this state was not created by a Android event -- ignore
        return;
      }

      // increase count of tags in this branch
      depth++;

      // can we start tagging??
      if (depth > mStartAfter) {
        // increase count of tagged states
        count++;

        // cache stateInfo
        StateInfo si = DebugMarkedJenkinsStateSet.si;
        si.mId = search.getStateId();
        si.mTag = mTag;

        //compare to previous state
        if (prevState != null) {
          try {
            si.compare(prevState, mWriter);
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
        prevState = si;

        // have we reached max count of tagged states to report?
        if (count >= mCount) {
          search.terminate();
        }
      }

      // reset tag & objinfo
      mTag = null;
      DebugMarkedJenkinsStateSet.si = null;
      DebugMarkedJenkinsStateSet.mTag = null;
      mWriter.flush();
      
    } else {
      java.nio.file.Path ff = Paths.get("tmp/" + "state_" + mTag + "." + search.getStateId());
      try {
        Files.deleteIfExists(ff);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    if (search.isEndState()) {
      mWriter.println("\n=== REACHED END STATE " + search.getStateId() + " ===");
    }

  }

  @Override
  public void searchStarted(Search search) {
    super.searchStarted(search);
    this.registerListener(search.getVM().getJPF());
  }

  @Override
  public void searchFinished(Search search) {
    mWriter.flush();
    mWriter.close();
  }

  @Override
  public StateNode getStateExtension() {
    StateNode n = new StateNode();
    n.count = count;
    n.depth = depth;
    n.prevState = prevState;
    return n;
  }

  @Override
  public void restore(StateNode n) {
    count = n.count;
    depth = n.depth;
    prevState = n.prevState;
  }

  @Override
  public void registerListener(JPF jpf) {
    StateExtensionListener sel = new StateExtensionListener<StateNode>(this);
    jpf.addSearchListener(sel);
  }

}