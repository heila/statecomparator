/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The Java Pathfinder core (jpf-core) platform is licensed under the
 * Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */
package gov.nasa.jpf;

import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.vm.DebugMarkedJenkinsStateSet;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.NativePeer;
import gov.nasa.jpf.vm.StateCompareListener;
import gov.nasa.jpf.vm.StateSerializer;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.serialize.DebugMarkedCFSerializer;

/**
 * native peer class for programmatic JPF interface (that can be used inside of apps to verify - if you are
 * aware of the danger that comes with it)
 * 
 * this peer is a bit different in that it only uses static fields and methods because its use is supposed to
 * be JPF global (without classloader namespaces)
 */
public class JPF_gov_nasa_jpf_vm_StateComparator extends NativePeer {

  @MJI
  public static void markState__Ljava_lang_String_2__V(MJIEnv env, int clsObjRef, int sTag) {
    StateSerializer serializer = env.getVM().getSerializer();
    if (serializer instanceof DebugMarkedCFSerializer) {
      DebugMarkedJenkinsStateSet.markState(env.getStringObject(sTag));
      StateCompareListener.mTag = env.getStringObject(sTag);
    }
    ThreadInfo ti = env.getThreadInfo();
    ti.breakTransition("Testing");
  }

  @MJI
  public static void markState__Ljava_lang_String_2I__V(MJIEnv env, int clsObjRef, int sTag, int max) {
    StateSerializer serializer = env.getVM().getSerializer();
    if (serializer instanceof DebugMarkedCFSerializer) {
      DebugMarkedJenkinsStateSet.markState(env.getStringObject(sTag));
      StateCompareListener.mCount = max;
      StateCompareListener.mTag = env.getStringObject(sTag);

    }
    ThreadInfo ti = env.getThreadInfo();
    ti.breakTransition("Testing");
  }

  @MJI
  public static void markState__Ljava_lang_String_2II__V(MJIEnv env, int clsObjRef, int sTag, int max,
                                                         int startAfter) {
    StateSerializer serializer = env.getVM().getSerializer();
    if (serializer instanceof DebugMarkedCFSerializer) {
      DebugMarkedJenkinsStateSet.markState(env.getStringObject(sTag));
      StateCompareListener.mCount = max;
      StateCompareListener.mTag = env.getStringObject(sTag);
      StateCompareListener.mStartAfter = startAfter;
    }
    ThreadInfo ti = env.getThreadInfo();
    ti.breakTransition("Testing");
  }

}