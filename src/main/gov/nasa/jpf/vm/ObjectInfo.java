package gov.nasa.jpf.vm;

import gov.nasa.jpf.vm.serialize.DebugMarkedCFSerializer;

import java.io.PrintWriter;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * This class is used to track and store the state of a reference object in the heap for debugging. This can
 * either be of type "class" storing all static fields of a class, "object" storing instance field info and
 * values or "array" that is also a reference object but has an additional mList value storing the
 * references/primitive values of the array.
 * 
 * @author Heila
 * 
 * @see DebugMarkedCFSerializer
 * @see DebugMarkedJenkinsStateSet
 * @see StateCompareListener
 * @see StateMatchingExample
 *
 */
public class ObjectInfo extends Info {

  /** object, class or array */
  public String mType;

  /**
   * The fully qualified name of the class. For reference object e.g. "com.example.Object" for primitive e.g.
   * "int" for array "char[]" or "java.lang.Object[]".
   */
  public String mClassName;

  /**
   * If this object stores an array, contains a list of the primitive values or in the case of references
   * String reference starting with '@' e.g. "@11b"
   */
  public Object[] mList;

  /** Field name-value mapping for this object. E.g. after "int iTest = 5;" ==> name = "iTest" value=5 */
  public Map<String, Object> mFields = new TreeMap<String, Object>();

  /** The JPF memory reference of the object stored as an int value */
  public int mRef;

  public boolean mVisited = false;

  public boolean mNotMatching = false;

  public void addField(String name, Object value) {
    mFields.put(name, value);
  }

  public void addList(Object[] list) {
    mList = list;
  }

  /**
   * Compares the current ObjectInfo to oi passed to this method to see what differs. Writes the output to the
   * writer. The reference to an Object can change since Integer and String are immutable and new Objects need
   * to be created to change a field's value. We can thus not compare the reference here directly. The best we
   * can do is compare the type and classname of the object. In the case of a list we can also compare the
   * length of the length of the list.
   * 
   * @param oi
   * @param writer
   */
  public void compare(ObjectInfo oi, PrintWriter writer) {
    // true if no changes found in objects
    boolean match = true;

    if (mClassName.equals(oi.mClassName) && mType.equals(oi.mType)) {

      if (mType.equals("object") || mType.equals("class")) {

        // match fields
        for (Entry<String, Object> fieldOrigEntry : mFields.entrySet()) {
          Object fieldOrig = fieldOrigEntry.getValue();
          Object fieldNew = oi.mFields.get(fieldOrigEntry.getKey());

          // ignore since this is a reference field
          if (fieldOrig != null && fieldOrig instanceof String && ((String) fieldOrig).startsWith("@")) {
            if (fieldNew != null && fieldNew instanceof String && ((String) fieldNew).startsWith("@")) {
              continue;
            }
          }

          // if original field value is null
          if (fieldOrig == null || fieldOrig.equals("null")) {
            if (fieldNew != null && !fieldNew.equals("null")) {
              match = false;
            } else {
              continue;
            }
          } else if (fieldNew == null || fieldNew.equals("null")) {
            match = false;
          } else if (fieldOrig.equals(fieldNew)) {
            continue;
          }

          writer.println("FIELD " + fieldOrigEntry.getKey() + ": " + fieldOrig + " --- NOT MATCHING --- "
              + fieldNew);
          match = false;
          break;
        }

      } else if (mType.equals("array")) {
        if (mList.length != oi.mList.length) {
          match = false;
        } else {
          for (int i = 0; i < mList.length; i++) {
            Object obj = mList[i];
            Object obj2 = oi.mList[i];

            if (obj instanceof String && obj2 instanceof String && ((String) obj).startsWith("@")
                && ((String) obj2).startsWith("@")) {
              // skip - they are references
            } else {
              if (!obj.equals(obj2)) {
                match = false;
                break;
              }
            }
          }
        }
      }
    } else {
      writer.println("???");
    }

    if (!match) {
      writer.println("-------  OBJECTS NOT MATCHING -------");
      writer.println(this.toString() + "\n" + oi.toString());
      writer.println("-------------------------------------");

    }

  }

  // public boolean compare(ObjectInfo oi, PrintWriter writer) {
  // boolean match = true;
  //
  // if (mClassName.equals(oi.mClassName) && mType.equals(oi.mType)) {
  // if (mType.equals("object") || mType.equals("class")) {
  //
  //
  // } else if (mType.equals("array")) {
  //
  // }
  // } else {
  // match = false;
  // writer.println("Classname or type not matching for same reference");
  // }
  //
  // if (!match) {
  // writer.println("-------  OBJECTS NOT MATCHING -------");
  // writer.println(this.toString() + "\n" + oi.toString());
  // writer.println("-------------------------------------");
  //
  // }
  //
  // return match;
  // }

  @Override
  public String toString() {
    String s = String.format("@%x", mRef) + " object " + mClassName;
    if (mList != null) {
      s += ", mList=[";
      for (int i = 0; i < mList.length; i++) {
        Object obj = mList[i];
        s = s + obj.toString() + ",";
      }
      s += "]";
    }
    if (!mFields.isEmpty())
      s += " mFields=" + mFields;
    return s;
  }

}
