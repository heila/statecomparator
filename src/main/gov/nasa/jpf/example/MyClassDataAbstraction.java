package gov.nasa.jpf.example;

import gov.nasa.jpf.vm.serialize.AbstractionAdapter;

public class MyClassDataAbstraction extends AbstractionAdapter {
  @Override
  public int getAbstractValue(int data) {
    if (data >= 4)
      return 4;
    return data;
  }
}