/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The Java Pathfinder core (jpf-core) platform is licensed under the
 * Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package gov.nasa.jpf.vm.serialize;

import gov.nasa.jpf.util.FinalBitSet;
import gov.nasa.jpf.vm.ArrayFields;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.DebugMarkedJenkinsStateSet;
import gov.nasa.jpf.vm.DebugStateSerializer;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.JPFOutputStream;
import gov.nasa.jpf.vm.LocalVarInfo;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.NativeStateHolder;
import gov.nasa.jpf.vm.ObjectInfo;
import gov.nasa.jpf.vm.ReferenceArrayFields;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.StackFrameInfo;
import gov.nasa.jpf.vm.StaticElementInfo;
import gov.nasa.jpf.vm.StaticInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Types;

import java.io.OutputStream;
import java.lang.reflect.Array;
import java.util.HashMap;

/**
 * a CFSerializer that stores the serialized program state in a readable/diffable format.
 * 
 * Automatically used by Debug..StateSet if the configured vm.storage.class is .vm.DebugJenkinsStateSet
 */
public class DebugMarkedCFSerializer extends CFSerializer implements DebugStateSerializer {

  JPFOutputStream os;
  boolean isHeap = false;
  boolean isClasses = false;

  // this is for debugging purposes only
  public DebugMarkedCFSerializer() {
    os = null;
  }

  @Override
  public void setOutputStream(OutputStream s) {
    if (s != null) {
      os = new JPFOutputStream(s);
    } else {
      os = null;
    }
  }

  @Override
  protected int[] computeStoringData() {
    if (os != null) {
      os.printCommentLine("------------------------ serialized state");
      isHeap = false;
      isClasses = false;
    }
    return super.computeStoringData();
  }

  @Override
  protected void processReferenceQueue() {
    if (os != null) {
      os.println();
      os.printCommentLine("--- heap");
      isHeap = true;
      isClasses = false;
      os.println();
    }
    super.processReferenceQueue();

  }

  @Override
  public void process(ElementInfo ei) {
    super.process(ei);
    if (os != null) {
      FinalBitSet filtered = !ei.isArray() ? getInstanceFilterMask(ei.getClassInfo()) : null;
      os.print(ei, filtered);
      os.println();
      if (isHeap) {
        // keep in current state object if listener
        ObjectInfo oi = new ObjectInfo();
        int ref = add(ei, filtered, oi);
        DebugMarkedJenkinsStateSet.si.dMap.put(ref, oi);
      } else if (isClasses) {
        // stores changes to classes in DebugMarkedJenkinsStateSet.mObjInfo "type == class"
        StaticInfo oi = new StaticInfo();
        int ref = add(ei, filtered, oi);
        DebugMarkedJenkinsStateSet.si.sMap.put(ref, oi);
      }
    }

  }

  @Override
  protected void serializeClassLoaders() {
    if (os != null) {
      os.println();
      os.printCommentLine("--- classes");
      os.println();
    }
    isHeap = false;
    isClasses = true;
    super.serializeClassLoaders();
  }

  @Override
  protected void serializeClass(StaticElementInfo sei) {
    super.serializeClass(sei);
    if (os != null) {
      FinalBitSet filtered = getStaticFilterMask(sei.getClassInfo());
      os.print(sei, filtered);
      os.println();
      if (isClasses) {
        // stores changes to classes in DebugMarkedJenkinsStateSet.mObjInfo "type == class"
        StaticInfo oi = new StaticInfo();
        int ref = add(sei, filtered, oi);
        DebugMarkedJenkinsStateSet.si.sMap.put(ref, oi);
      }
    }

  }

  @Override
  protected void serializeStackFrames() {
    if (os != null) {
      os.println();
      os.printCommentLine("--- threads");
      os.println();
    }
    super.serializeStackFrames();
  }

  @Override
  protected void serializeStackFrames(ThreadInfo ti) {
    if (os != null) {
      os.println();
      os.print(ti);
      os.println();
    }
    mTi = ti;
    super.serializeStackFrames(ti);
  }

  ThreadInfo mTi = null;

  @Override
  protected void serializeFrame(StackFrame frame) {
    if (os != null) {
      os.print(frame);
      os.println();

      StackFrameInfo oi = new StackFrameInfo();
      oi.depth = frame.getDepth();
      oi.mClassName = frame.getClassName();
      oi.mMethodName = frame.getMethodName();
      oi.threadID = mTi.getGlobalId();
      oi.mLocals = new HashMap<String, Object>();

      int[] slots = frame.getSlots();
      for (int i = 0; i < slots.length; i++) {
        LocalVarInfo info = frame.getLocalVarInfo(i);
        if (info != null) {
          String name = info.getName();
          Object value = null;
          if (frame.isReferenceSlot(i))
            value = "@" + Integer.toHexString(frame.getLocalVariable(i));
          else
            value = frame.getLocalValueObject(info);
          oi.mLocals.put(name, value);
        }
      }
      DebugMarkedJenkinsStateSet.si.fMap.add(oi);
    }
    super.serializeFrame(frame);
  }

  @Override
  protected void serializeNativeStateHolders() {
    if (os != null) {
      os.println();
      os.printCommentLine("--- native state holders");
      os.println();
      isHeap = false;
      isClasses = false;
    }
    super.serializeNativeStateHolders();
  }

  @Override
  protected void serializeNativeStateHolder(NativeStateHolder nsh) {
    if (os != null) {
      os.print(nsh);
      os.println();
    }
    super.serializeNativeStateHolder(nsh);
  }

  boolean useSid = false;
  int maxElements = -1;

  public int add(ElementInfo ei, FinalBitSet filterMask, ObjectInfo oi) {

    boolean isObject = ei.isObject();
    ClassInfo ci = ei.getClassInfo();

    int ref = (useSid) ? ei.getSid() : ei.getObjectRef();
    oi.mRef = ref;
    if (isObject) {
      oi.mType = "object";
      if (ei.isArray()) {
        oi.mType = "array";
        oi.mClassName = Types.getTypeName(ci.getName());
      } else {
        oi.mClassName = ci.getName();
      }
    } else {
      oi.mType = "class";
      oi.mClassName = ci.getName();
    }

    if (isObject) {
      if (ei.isArray()) {
        ArrayFields af = ei.getArrayFields();
        addArrayList(oi, af);
      } else {
        addFields(ei, ci.getInstanceFields(), filterMask, oi);
      }

    } else {
      addFields(ei, ci.getDeclaredStaticFields(), filterMask, oi);
    }

    return ref;
  }

  protected void addFields(ElementInfo ei, FieldInfo[] fields, FinalBitSet filterMask, ObjectInfo oi) {
    if (fields != null) {
      for (int i = 0; i < fields.length; i++) {
        add(ei, fields[i], (filterMask != null && filterMask.get(i)), oi);
      }
    }
  }

  public void add(ElementInfo ei, FieldInfo fi, boolean isFiltered, ObjectInfo oi) {
    String name = fi.getName();
    if (isFiltered) {
      oi.addField(name, "X");
    } else {
      switch (fi.getTypeCode()) {
      case Types.T_BOOLEAN:
        oi.addField(name, ei.getBooleanField(fi));
        break;
      case Types.T_BYTE:
        oi.addField(name, ei.getByteField(fi));
        break;
      case Types.T_CHAR:
        oi.addField(name, ei.getCharField(fi));
        break;
      case Types.T_SHORT:
        oi.addField(name, ei.getShortField(fi));
        break;
      case Types.T_INT:
        oi.addField(name, ei.getIntField(fi));
        break;
      case Types.T_LONG:
        oi.addField(name, ei.getLongField(fi));
        break;
      case Types.T_FLOAT:
        oi.addField(name, ei.getFloatField(fi));
        break;
      case Types.T_DOUBLE:
        oi.addField(name, ei.getDoubleField(fi));
        break;

      case Types.T_REFERENCE:
      case Types.T_ARRAY:

        if (ei.getReferenceField(fi) == MJIEnv.NULL) {
          oi.addField(name, "null");
        } else {
          oi.addField(name, String.format("@%x", ei.getReferenceField(fi)));
        }
        break;
      }
    }
  }

  public void addArrayList(ObjectInfo oi, ArrayFields af) {
    // get value
    Object[] obj = null;
    if (af instanceof ReferenceArrayFields) {
      int[] v = ((ReferenceArrayFields) af).asReferenceArray();
      obj = new String[v.length];
      for (int i = 0; i < v.length; i++) {
        obj[i] = String.format("@%x", v[i]);
      }
    } else {
      Object o = af.getValues();
      obj = new Object[af.arrayLength()];
      for (int i = 0; i < af.arrayLength(); i++) {
        obj[i] = Array.get(o, i);
      }
    }
    oi.mList = obj;
  }
}
