package gov.nasa.jpf.vm;

public class StaticInfo extends ObjectInfo {

  @Override
  public String toString() {
    String s = String.format("@%x", mRef) + " class " + mClassName;
    if (mList != null) {
      s += " list=[";
      for (int i = 0; i < mList.length; i++) {
        Object obj = mList[i];
        s = s + obj.toString() + ",";
      }
      s += "]";
    }
    s += " fields=" + mFields;
    return s;
  }

}
