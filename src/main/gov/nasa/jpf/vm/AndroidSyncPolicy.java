package gov.nasa.jpf.vm;

import gov.nasa.jpf.Config;

public class AndroidSyncPolicy extends AllRunnablesSyncPolicy {

  public AndroidSyncPolicy(Config config) {
    super(config);
  }

  @Override
  protected ThreadInfo[] getTimeoutRunnables(ApplicationContext appCtx) {
    ThreadInfo[] allRunnables = super.getTimeoutRunnables(appCtx);

    int maxPrio = Integer.MIN_VALUE;
    int n = 0;
    for (int i = 0; i < allRunnables.length; i++) {
      ThreadInfo ti = allRunnables[i];
      if (ti != null) {
        int prio = ti.getPriority();

        if (prio > maxPrio) {
          maxPrio = prio;
          for (int j = 0; j < i; j++) {
            allRunnables[j] = null;
          }
          n = 1;

        } else if (prio <= maxPrio) {
          allRunnables[i] = null;
        }
      }
    }

    if (n < allRunnables.length) {
      ThreadInfo[] prioRunnables = new ThreadInfo[n];
      for (int j = 0; j < allRunnables.length; j++) {
        if (allRunnables[j] != null) {
          prioRunnables[0] = allRunnables[j];
          break;
        }
      }

      return prioRunnables;

    } else {
      return allRunnables;
    }
  }
}
