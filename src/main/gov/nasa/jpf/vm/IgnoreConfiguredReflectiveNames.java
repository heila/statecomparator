/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The Java Pathfinder core (jpf-core) platform is licensed under the
 * Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package gov.nasa.jpf.vm;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.vm.serialize.FieldAmmendmentByName;

public class IgnoreConfiguredReflectiveNames extends FieldAmmendmentByName {

  static final String[] reflectiveNameFields = {
  // "com.keepassdroid.Database.groups"
  // "com.nloko.android.syncmypix.facebook.FacebookSyncService.mCropSquare",
  // "com.nloko.android.syncmypix.facebook.FacebookSyncService.mCacheOn"
  };

  static final String[] ignoreFields = initializeFields();

  private static String[] initializeFields() {
    Config c = VM.getVM().getConfig();
    String[] ignoreList = c.getStringArray("filter.ignoreFields");
    String[] combined = {};
    if (ignoreList != null && ignoreList.length > 0) {
      combined = concat(reflectiveNameFields, ignoreList);
    } else {
      combined = reflectiveNameFields;
    }
    return combined;
  }

  public static String[] concat(String[] a1, String[] a2) {
    int a1Len = a1.length;
    int a2Len = a2.length;
    String[] c = new String[a1Len + a2Len];
    System.arraycopy(a1, 0, c, 0, a1Len);
    System.arraycopy(a2, 0, c, a1Len, a2Len);
    return c;
  }

  public IgnoreConfiguredReflectiveNames() {
    super(ignoreFields, POLICY_IGNORE);
  }

  // must be at bottom!
  public static final IgnoreConfiguredReflectiveNames instance = new IgnoreConfiguredReflectiveNames();
}
