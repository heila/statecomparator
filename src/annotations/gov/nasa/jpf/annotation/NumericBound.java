package gov.nasa.jpf.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that a field in the model should not be considered during state matching.
 * 
 * @author peterd
 */
@Retention(value = java.lang.annotation.RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.LOCAL_VARIABLE })
public @interface NumericBound {

  /**
   * If not the empty string, specifies a property that must be "true" to activate this filter--unless
   * <code>invert</code> is set.
   */
  double max() default Double.MAX_VALUE;

  /**
   * If not the empty string, specifies a property that must be "true" to activate this filter--unless
   * <code>invert</code> is set.
   */
  double min() default Double.MIN_VALUE;

}
