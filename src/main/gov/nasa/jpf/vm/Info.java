package gov.nasa.jpf.vm;

import java.io.PrintWriter;

public class Info {

  public Info mParent;
  public boolean mMatch = true;

  public void printTrace(PrintWriter pw) {
    Info parent = this;
    while (parent != null) {
      pw.println(parent.toString());
      parent = parent.mParent;
    }

  }
}
