/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The Java Pathfinder core (jpf-core) platform is licensed under the
 * Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */
package gov.nasa.jpf;

import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.vm.DebugMarkedJenkinsStateSet;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.NativePeer;
import gov.nasa.jpf.vm.StateCompareListener;
import gov.nasa.jpf.vm.StateSerializer;
import gov.nasa.jpf.vm.serialize.DebugMarkedCFSerializer;

/**
 * Same as StateComparator but does not break transitions
 */
public class JPF_gov_nasa_jpf_vm_AndroidStateComparator extends NativePeer {

  @MJI
  public static void markState__Ljava_lang_String_2__V(MJIEnv env, int clsObjRef, int sTag) {
    StateSerializer serializer = env.getVM().getSerializer();
    if (serializer instanceof DebugMarkedCFSerializer) {
      StateCompareListener.isAndroid = true;
      DebugMarkedJenkinsStateSet.markState(env.getStringObject(sTag));
      StateCompareListener.mTag = env.getStringObject(sTag);
    }
  }

  @MJI
  public static void markState__Ljava_lang_String_2I__V(MJIEnv env, int clsObjRef, int sTag, int max) {
    StateSerializer serializer = env.getVM().getSerializer();
    if (serializer instanceof DebugMarkedCFSerializer) {
      StateCompareListener.isAndroid = true;
      DebugMarkedJenkinsStateSet.markState(env.getStringObject(sTag));
      StateCompareListener.mCount = max;
      StateCompareListener.mTag = env.getStringObject(sTag);
    }
  }

  @MJI
  public static void markState__Ljava_lang_String_2II__V(MJIEnv env, int clsObjRef, int sTag, int max,
                                                         int startAfter) {
    StateSerializer serializer = env.getVM().getSerializer();
    if (serializer instanceof DebugMarkedCFSerializer) {
      StateCompareListener.isAndroid = true;
      DebugMarkedJenkinsStateSet.markState(env.getStringObject(sTag));
      StateCompareListener.mCount = max;
      StateCompareListener.mTag = env.getStringObject(sTag);
      StateCompareListener.mStartAfter = startAfter;
    }
  }

}